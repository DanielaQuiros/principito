﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{
	public GameObject player;
	public Vector3 coord;
    // Start is called before the first frame update
    void Start()
    {
        coord = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = player.transform.position + coord;
    }
}
