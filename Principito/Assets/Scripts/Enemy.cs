﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
	public float distance; 
	public NavMeshAgent agent; 
	public GameObject target; 
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(target.transform.position, transform.position) < distance) 
		{
			agent.SetDestination(target.transform.position);
		}else{
			agent.speed = 5;
		}
    }
}
