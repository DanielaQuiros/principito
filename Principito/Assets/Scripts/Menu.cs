﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.EventSystems;

public class Menu : MonoBehaviour
{

    private bool pausa,estado; //determina cuando se pausa el juego
    private GameObject menu, panel,boton;
    string nombre;
    int life;
   
    // Use this for initialization
    public void Start()
    {
        estado = false;
        pausa = false;
        Pausa();
        Activar();
       // life = 500;
       
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Desert");
    }

    public void showPanel(bool state)
    {
        if (SceneManager.GetActiveScene().name != "Inicio")
        {
            menu = gameObject.transform.Find("pnl_menu").gameObject;
            menu.SetActive(state);
        }
    }

    public void showStartMenu()
    {
        SceneManager.LoadScene("Inicio");
    }

    public void Pausa() //Pausa el juego
    {
        if (pausa)
        {
            Time.timeScale = 0;
            showPanel(true);
            pausa = false;
        }

        else if (!pausa)
        {
            Time.timeScale = 1;
            showPanel(false);
            pausa = true;
        }
    }
    public void showP(bool est)
    {
        panel = gameObject.transform.Find("pnl_pregunta").gameObject;
        panel.SetActive(est);
        string n;
        if (est)
        {
            
            switch (Random.Range(1, 4))
            {
                case 1:
                    GameObject.Find("txt_p").GetComponent<Text>().text = "'Solo se puede ver bien con ________, /nlo esencial es invisble a __________'";
                    GameObject.Find("txt_op1").GetComponent<Text>().text = "el alma,el corazon";
                    GameObject.Find("txt_op2").GetComponent<Text>().text = "el corazon,los ojos";
             

                 
                    break;
                case 2:
                    GameObject.Find("txt_p").GetComponent<Text>().text = "'Debi juzagrla por sus actos y no por sus palabras' ¿A quien se refiere la frase?";
                    GameObject.Find("txt_op1").GetComponent<Text>().text = "la rosa";
                    GameObject.Find("txt_op2").GetComponent<Text>().text = "la serpiente";
                    break;
                case 3:
                    GameObject.Find("txt_p").GetComponent<Text>().text = "'¿Que representa el dibujo #2?'";
                    GameObject.Find("txt_op1").GetComponent<Text>().text = "Un sombrero";
                    GameObject.Find("txt_op2").GetComponent<Text>().text = "Una serpiente que se comio un elefante";
                    break;


            }
        }
        

    }
    public void Click()
    {
        nombre = EventSystem.current.currentSelectedGameObject.name;
        print(nombre);
    }
    public void Activar()
    {
        if (estado)
        {
            Time.timeScale = 0;
            showP(true);
            estado = false;
        }

        else if (!estado)
        {
            Time.timeScale = 1;
            showP(false);
            estado = true;
        }
    }
}
