﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jugador : MonoBehaviour
{
    private Rigidbody rigidbody;
    public float velocidad;
    private int contador;
	public Animator controlador;
	float translation;
	int life;
	bool left,right;
    private int catAves = 0;
	

    void Start()
    {
		controlador = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody>();
        contador = 0;
		life = 500;
    }
    void Update()
    {
		if(life >= 100)
		{
			translation = Time.deltaTime * 5f;
            if (Input.GetKey(KeyCode.D))// || Input.GetAxis("Horizontal") == 1)
            {
                controlador.Play("Walk");
                transform.position += Vector3.right * translation;
                rigidbody.position += Vector3.right * velocidad * Time.deltaTime;
                transform.rotation = Quaternion.Euler(0, 90, 0);
            }
            else
            if (Input.GetKey(KeyCode.A) )//|| Input.GetAxis("Horizontal") == -1)
            {
                controlador.Play("Walk");
                transform.position += Vector3.left * translation;
                rigidbody.position += Vector3.left * velocidad * Time.deltaTime;
                transform.rotation = Quaternion.Euler(0, -90, 0);
            }
            else
            if (Input.GetKey(KeyCode.W))// || Input.GetAxis("Vertical") == 1)
            {
                controlador.Play("Walk");
                transform.position += Vector3.forward * translation;
                rigidbody.position += Vector3.forward * velocidad * Time.deltaTime;
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else
            if (Input.GetKey(KeyCode.S))// || Input.GetAxis("Vertical") == -1)
            {
                controlador.Play("Walk");
                transform.position += Vector3.back * translation;
                rigidbody.position += Vector3.back * velocidad * Time.deltaTime;
                transform.rotation = Quaternion.Euler(0, -180, 0);
            }
            else
            if(Input.GetKey(KeyCode.J))// || Input.GetKey(KeyCode.JoystickButton0))
            {
                controlador.Play("Jump");
            }
            else
                controlador.Play("Idle");
        }
        else
        {
            GameObject.Find("txt_fin").GetComponent<Text>().text = "GAME OVER!";
            Time.timeScale = 0;
        }

    }
    void OnCollisionEnter(Collision obj)
    {
        if (obj.gameObject.tag == "Potion")
        {
            life += 100;
            GameObject barra = GameObject.Find("life_bar");
            barra.GetComponent<RectTransform>().sizeDelta = new Vector2(life,25);
            Destroy(obj.gameObject);
            contador++;
        }
        if (contador == 10)
        {

        }
        if (obj.gameObject.tag == "Bird")
        {
            catAves++;
            Destroy(obj.gameObject);
        }
        if (obj.gameObject.tag == "Snake")
        {
            life -= 100;
            GameObject barra = GameObject.Find("life_bar");
            barra.GetComponent<RectTransform>().sizeDelta = new Vector2(life, 25);

        }
    }

}
